#!/usr/bin/env python   
# -*- coding: ISO-8859-1 -*-

'''
sku wrote this program. As long as you retain this notice you
can do whatever you want with this stuff. If we meet some day, and you think
this stuff is worth it, you can buy me a beer in return.
'''


from os import system as OMGDONT
from ModifierList import getModifier, getModifierName
from ItemList import getItem, getItemName
from NotifyItems import shouldNotify, isSearchItem, isFlaskItem, isQuestItem, isQuiverItem, isShieldItem, isBeltItem, isGemItem, isArmourItem, isCurrencyItem, isMapItem, isJewelleryItem, getSocketColor
from ByteBuffer import ByteBuffer
import ctypes
import sys
import threading
import winsound
import atexit
import datetime
import traceback

from colorama import init, Fore, Back, Style

init(autoreset=True)

try:
    from pydbg import *
    from pydbg.defines import *
except:
    print 'You seem to be missing pydbg or pydasm.'
    print 'Precompiled binaries can be downloaded from here: http://www.lfd.uci.edu/~gohlke/pythonlibs/#pydbg'
    sys.exit(1)

ALERT_VERSION = 'Sep-11-2014'
POE_VERSION = '1.2.2'

###########################################################################################
# Configuration for itemalerter. Only change these values.
###########################################################################################

# Debug settings#
DEBUG = False # Logs output, also alerts to unknown items (in console and with sound)
DEBUG_ALL = False #logs player joining, leaving, packets unrelated to loot

# Steam or standalone version of Path of Exile
STEAM = False

ALIGN = 0x00400000

################
# Console Alerts
################
ALERT_rares = True
ALERT_quest = True # Alert for quest drops (medicine chest etc)
ALERT_gems = True # Alert for special gems & superior gems
ALERT_specialgems = True #portal/multistrike
ALERT_quality = True # alert for quality equipment
ALERT_flask = True # Alert for superior flasks & Granite Flask
ALERT_maps = True
ALERT_curr = True #currency alerts
ALERT_5slot5link = True
ALERT_6slot = True
ALERT_6slot5link = True
ALERT_6slot6link = True
ALERT_jewellry = True #jewelry (gold amulet, etc)
ALERT_race = False #race sockets (RRR, GGG, BBB)

##############
# Sound Alerts
##############
SOUND_curr = True # Currency sounds, needs to be on to play any/all
SOUND_rares = False
SOUND_quest = False
SOUND_superior = True #Alert for superior equipment
SOUND_supflask = True 
SOUND_supgem = True # Alert for superior quality gems
SOUND_uniques = True
SOUND_slots = True # Alert if 5/6S and 5/6L.
SOUND_specialgems = True # Alert if portal/multistrike
SOUND_maps = True
SOUND_rgb = True # Alert for RGB (Chromatic vendor recipe)
SOUND_race = False # Alert if race sockets
SOUND_graniteflask = False

################
#Currency Sounds
################
SOUND_eternal = True
SOUND_exalted = True
SOUND_divine = True
SOUND_gcp = True
SOUND_regal = True
SOUND_regret = True
SOUND_vaal = True
SOUND_chaos = True
SOUND_blessed = True
SOUND_scour = True
SOUND_alc = True
SOUND_fuse = True
SOUND_chis = True
SOUND_chroma = True
SOUND_chance = True
SOUND_jew = True
SOUND_glass = True
SOUND_mirror = True
SOUND_feather = True # Albino Rhoa Feather
SOUND_alter = False
SOUND_trans = False
SOUND_augmen = False

# Quality Modifiers & Other misc configurations
ringmf = 6 # 6% is lowest a ring can have (0 to disable)
amuletmf = 12 # 12% is the lowest an amulet can have (0 to disable)
whiteitemqual = 20 # White Equipment quality modifier (0 to disable)
rarityqual = 20 # Magic/Rare/Unique equipment quality modifier (0 to disable)
gemqual = 5 # Gem quality modifier
flaskqual = 5 # Flask quality modifier
specialGems = '0x4C32EEEE,0x26856055' # Special gems. (Multistrike and Portal)
race_sockets = ('R-R-R', 'G-G-G', 'B-B-B') # Special race related sockets.


################################
## UNUSED or Work-In-Progress ##
################################

#SHOW_OWN_ITEMS_ONLY = True

#Search special item, do NOT set to True, the required ilvl doesnt always work
SEARCH_special = False
special_class = 'BootsStr'
special_mod = 'Armour'
special_ilvl = 28
special_rlvl = 24
################################



###########################################################################################
# END OF CONFIG. Do not change whats below here unless you know what you are doing 
###########################################################################################

RUN_START = 0
RUN_END = 0

RUN = False

number_of_uniques = 0
number_of_rares = 0
number_of_orbs = 0
number_of_maps = 0

class PlaySoundGraniteFlask(threading.Thread):
    def run(self):
        winsound.PlaySound(r'sounds\GraniteFlask.wav', winsound.SND_FILENAME)

class PlaySoundSup(threading.Thread):
    def run(self):
        winsound.PlaySound(r'sounds\SuperiorItem.wav', winsound.SND_FILENAME)

class PlaySoundFeather(threading.Thread):
    def run(self):
        winsound.PlaySound(r'sounds\Feather.wav', winsound.SND_FILENAME)

class PlaySoundChaos(threading.Thread):
    def run(self):
        winsound.PlaySound(r'sounds\ChaosOrb.wav', winsound.SND_FILENAME)

class PlaySoundRace(threading.Thread):
    def run(self):
        winsound.PlaySound(r'sounds\RaceItem.wav', winsound.SND_FILENAME)

class PlaySoundRGB(threading.Thread):
    def run(self):
        winsound.PlaySound(r'sounds\RGB.wav', winsound.SND_FILENAME)

class PlaySoundMaps(threading.Thread):
    def run(self):
        winsound.PlaySound(r'sounds\MapItem.wav', winsound.SND_FILENAME)

class PlaySound6Sockets(threading.Thread):
    def run(self):
        winsound.PlaySound(r'sounds\SixSockets.wav', winsound.SND_FILENAME)

class PlaySoundSixLink(threading.Thread):
    def run(self):
        winsound.PlaySound(r'sounds\SixLink.wav', winsound.SND_FILENAME)

class PlaySoundFiveLink(threading.Thread):
    def run(self):
        winsound.PlaySound(r'sounds\FiveLink.wav', winsound.SND_FILENAME)

class PlaySoundSixSlotFiveLink(threading.Thread):
    def run(self):
        winsound.PlaySound(r'sounds\SixSlotFiveLink.wav', winsound.SND_FILENAME)

class PlaySoundCraftingItem(threading.Thread):
    def run(self):
        winsound.PlaySound(r'sounds\CraftingItem.wav', winsound.SND_FILENAME)

class PlaySoundWorker(threading.Thread):
    def run(self):
        winsound.PlaySound(r'sounds\RaceItem.wav', winsound.SND_FILENAME)

class PlaySoundUnique(threading.Thread):
    def run(self):
        winsound.PlaySound(r'sounds\UniqueItem.wav', winsound.SND_FILENAME)

class PlaySoundSuperiorGem(threading.Thread):
    def run(self):
        winsound.PlaySound(r'sounds\SuperiorGem.wav', winsound.SND_FILENAME)

class PlaySoundExalted(threading.Thread):
    def run(self):
        winsound.PlaySound(r'sounds\ExaltedOrb.wav', winsound.SND_FILENAME)

class PlaySoundEternal(threading.Thread):
    def run(self):
        winsound.PlaySound(r'sounds\EternalOrb.wav', winsound.SND_FILENAME)

class PlaySoundKalandra(threading.Thread):
    def run(self):
        winsound.PlaySound(r'sounds\Eternalkalandra.wav', winsound.SND_FILENAME)

class PlaySoundHolyShit(threading.Thread):
    def run(self):
        winsound.PlaySound(r'sounds\HolyShit.wav', winsound.SND_FILENAME)

class PlaySoundQuest(threading.Thread):
    def run(self):
        winsound.PlaySound(r'sounds\QuestItem.wav', winsound.SND_FILENAME)

class PlaySoundSuperiorFlask(threading.Thread):
    def run(self):
        winsound.PlaySound(r'sounds\SuperiorFlask.wav', winsound.SND_FILENAME)

class PlaySoundUnknownItem(threading.Thread):
    def run(self):
        winsound.PlaySound(r'sounds\UnknownItem.wav', winsound.SND_FILENAME)

class PlaySoundSpecialItem(threading.Thread):
    def run(self):
        winsound.PlaySound(r'sounds\SpecialItem.wav', winsound.SND_FILENAME)

class PlaySoundVaal(threading.Thread):
    def run(self):
        winsound.PlaySound(r'sounds\VaalOrb.wav', winsound.SND_FILENAME)

class PlaySoundDivine(threading.Thread):
    def run(self):
        winsound.PlaySound(r'sounds\DivineOrb.wav', winsound.SND_FILENAME)

##############################
#### Not in use currently ####
##############################
#class SoundPlayer(threading.Thread):
#    def run(self, sound):
#        winsound.PlaySound(r'sounds\\' + sound, winsound.SND_FILENAME)
#        print >>self.logFile, str.format('Playing: {0!s}', sound)

##############################

class ItemAlert(object):

####################################
#               1.2.1b
    if not STEAM:
        BP0 = 0x002C2A89 + ALIGN
        BP1 = 0x002C2A81 + ALIGN
        BP2 = 0x002C2ACB + ALIGN

    else:

        BP0 = 0x002C4189 + ALIGN
        BP1 = 0x002C4181 + ALIGN
        BP2 = 0x002C568B + ALIGN

####################################
#               1.2.1b
#    if not STEAM:
#
#        BP0 = 0x002C1489 + ALIGN
#        BP1 = 0x002C1481 + ALIGN
#        BP2 = 0x006C2ACB + ALIGN
#
#    else:
#
#        BP0 = 0x002C4189 + ALIGN
#        BP1 = 0x002C4181 + ALIGN
#        BP2 = 0x002C41CB + ALIGN
#    
######################################
#               HISTORY
######################################
####################################
#               1.1.3_r2
#    if not STEAM:
#
#        BP0 = 0x00285FA9 + ALIGN
#        BP1 = 0x00285FA1 + ALIGN
#        BP2 = 0x00285FEB + ALIGN
#
#    else:
#
#        BP0 = 0x00287B79 + ALIGN
#        BP1 = 0x00287B71 + ALIGN
#        BP2 = 0x00287BBB + ALIGN
######################################
#               1.1.3_r2
#    Non-Steam
#        BP0 = 0x00286079 + 0x00400000
#        BP1 = 0x00286071 + 0x00400000
#        BP2 = 0x002860bb + 0x00400000
#    Steam
#         BP0 = 0x00287B09 + 0x00400000
#        BP1 = 0x00287B01 + 0x00400000
#        BP2 = 0x00287B4B + 0x00400000
######################################
#               1.1.3
#    Non-Steam
#        BP0 = 0x00285349 + 0x00400000
#        BP1 = 0x00285341 + 0x00400000
#        BP2 = 0x0028538B + 0x00400000
#    Steam
#        BP0 = 0x002881F9 + 0x00400000
#        BP1 = 0x002881F1 + 0x00400000
#        BP2 = 0x0028823B + 0x00400000
######################################
#               1.1.2b
#   Non-Steam
#        BP0 = 0x00282719 + 0x00400000
#        BP1 = 0x00282711 + 0x00400000
#        BP2 = 0x0028275B + 0x00400000
#   Steam
#        BP0 = 0x002850D9 + 0x00400000
#        BP1 = 0x002850D1 + 0x00400000
#        BP2 = 0x0028511B + 0x00400000
######################################
#               1.1.2
#   Non-Steam
#        BP0 = 0x00282619 + 0x00400000
#        BP1 = 0x00282611 + 0x00400000
#        BP2 = 0x0028265B + 0x00400000
#   Steam
#        BP0 = 0x00285129 + 0x00400000
#        BP1 = 0x00285121 + 0x00400000
#        BP2 = 0x0028516B + 0x00400000
######################################
#               1.1.1c_r2
#   Non-Steam
#        BP0 = 0x00280E49 + 0x00400000
#        BP1 = 0x00280E41 + 0x00400000
#        BP2 = 0x00280E8B + 0x00400000
#   Steam
#        BP0 = 0x002842D9 + 0x00400000
#        BP1 = 0x002842D1 + 0x00400000
#        BP2 = 0x0028431B + 0x00400000
######################################
#               1.1.1c
#    Non-Steam
#        BP0 = 0x002814D9 + 0x00400000
#        BP1 = 0x002814D1 + 0x00400000
#        BP2 = 0x0028151B + 0x00400000
#    Steam
#        BP0 = 0x00283FF9 + 0x00400000
#        BP1 = 0x00283FF1 + 0x00400000
#        BP2 = 0x0028403B + 0x00400000
######################################
#               1.1.1b
#   Non-Steam
#        BP0 = 0x00280CA9 + 0x00400000
#        BP1 = 0x00280CA1 + 0x00400000
#        BP2 = 0x00280CEB + 0x00400000
#   Steam
#        BP0 = 0x00284169 + 0x00400000
#        BP1 = 0x00284161 + 0x00400000
#        BP2 = 0x002841AB + 0x00400000
######################################
#               1.1.1
#   Non-Steam
#        BP0 = 0x002806D9 + 0x00400000
#        BP1 = 0x002806D1 + 0x00400000
#        BP2 = 0x0028071B + 0x00400000
#   Steam
#        BP0 = 0x00283479 + 0x00400000
#        BP1 = 0x00283471 + 0x00400000
#        BP2 = 0x002834BB + 0x00400000
######################################
#               1.1.0e/f
#    Non-Steam
#        BP0 = 0x002804A9 + 0x00400000
#        BP1 = 0x002804A1 + 0x00400000
#        BP2 = 0x002804EB + 0x00400000
#    Steam
#        BP0 = 0x00283269 + 0x00400000
#        BP1 = 0x00283261 + 0x00400000
#        BP2 = 0x002832AB + 0x00400000
######################################
#               1.1.0d
#   Non-Steam     
#        BP0 = 0x0027FEE9 + 0x00400000
#        BP1 = 0x0027FEE1 + 0x00400000
#        BP2 = 0x0027FF2B + 0x00400000
#   Steam
#        BP0 = 0x00282B99 + 0x00400000
#        BP1 = 0x00282B91 + 0x00400000
#        BP2 = 0x00282BDB + 0x00400000
######################################

    number_of_uniques = 0
    number_of_rares = 0
    number_of_orbs = 0
    number_of_maps = 0
    
    RUN = False
    
    def __init__(self):
        atexit.register(self.atExit)
        self.logFile = open('log.txt', 'a', 0)
        self.statFile = open('stats.txt', 'a', 0)
        print >>self.logFile, 40 * '='
        print >>self.logFile, str.format('Started ItemAlertPoE version {0} at {1!s}.', ALERT_VERSION, datetime.datetime.now())
        print >>self.logFile, str.format('Python version: {0!s}', sys.version_info)
        self.dbg = pydbg()
        self.dbg.attach(self.getProcessId())
        print Style.BRIGHT + Fore.CYAN + 'Success'
        print ''
        self.baseAddress = self.getBaseAddress()
        adjustment = self.baseAddress - 0x00400000
        ItemAlert.BP0 += adjustment
        ItemAlert.BP1 += adjustment
        ItemAlert.BP2 += adjustment
        self.lastPacketBufferAddress = 0
        self.lastPacketSize = 0

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        #print ''
        #print '__exit__'
        #print ''
        print Fore.RED + 'Unloading'
        self.dbg.detach()
        self.logFile.close()
        self.statFile.close()

    def grabPacketSize(self, dbg):
        self.lastPacketSize = dbg.get_register('eax')
        return DBG_CONTINUE

    def beforeDemanglingPacket(self, dbg):
        self.lastPacketBufferAddress = dbg.get_register('eax')
        return DBG_CONTINUE


    def playerJoined(self,packetData): 
    
        try:
        
            buffer = ByteBuffer(packetData)
            buffer.setEndian(ByteBuffer.BIG_ENDIAN)

            id = buffer.nextByte()
            unk1 = buffer.nextDword()
            unk2 = buffer.nextByte()
            player_name_len = buffer.nextByte()
            
            playername = buffer.getString(player_name_len)
            
            print Fore.WHITE + str.format('Player joined the area: {0}',playername)
            return
            
        except:pass
        
    def playerLeft(self,packetData): 
    
        try:
        
            buffer = ByteBuffer(packetData)
            buffer.setEndian(ByteBuffer.BIG_ENDIAN)

            id = buffer.nextByte()
            unk1 = buffer.nextDword()
            unk2 = buffer.nextByte()
            player_name_len = buffer.nextByte()
            
            playername = buffer.getString(player_name_len)
            
            print Fore.WHITE + str.format('Player left the area: {0}',playername)
            return
            
        except:pass

    def playerChat(self, packetData): 
    
        try:
            
            buffer = ByteBuffer(packetData)
            buffer.setEndian(ByteBuffer.BIG_ENDIAN)

            id = buffer.nextByte()
            
            unk1 = buffer.nextByte()
            
            player_name_len = buffer.nextByte()
            
            playername = buffer.getString(player_name_len)
            #print Fore.WHITE + str.format('Player is chatting: {0}',playername)
            return
        
        except: pass

    def parseWorldItemPacket(self, packetData):
    
        global number_of_uniques
        global number_of_rares 
        global number_of_orbs 
        global number_of_maps
        global RUN
    
        try:
            ts = datetime.datetime.now().strftime("%H:%M:%S") + ' '
            buffer = ByteBuffer(packetData)
            buffer.setEndian(ByteBuffer.BIG_ENDIAN)

            id = buffer.nextByte()
            #print >>self.logFile, str.format('id = {0}', id)

            objectType = buffer.nextDword()
            #print >>self.logFile, str.format('ObjectType = {0}', objectType)

            unk1 = buffer.nextDword()
            #print >>self.logFile, str.format('unk1 = {0}', unk1)

            unk2 = buffer.nextByte()
            #print >>self.logFile, str.format('unk2 = {0}', unk2)

            if unk2 != 0: 
                print >>self.logFile, 'The following packet has an odd unk2 field:'
                print >>self.logFile, self.dbg.hex_dump(map(lambda x: chr(x), packetData))
                return

            xcoord = buffer.nextDword()
            #print >>self.logFile, str.format('x coord = {0}', xcoord)
            
            ycoord = buffer.nextDword()
            #print >>self.logFile, str.format('y coord = {0}', ycoord)
            
            rot = buffer.nextDword()
            #print >>self.logFile, str.format('rot = {0}', rot)

            unk3 = buffer.nextDword(ByteBuffer.LITTLE_ENDIAN)
            #print >>self.logFile, str.format('unk3 = {0}', unk3)
            
            unk4 = buffer.nextDword()
            #print >>self.logFile, str.format('unk4 = {0}', unk4)
            
            if unk3 >> 2 != 0:
                print >>self.logFile, 'The following packet has an odd unk3 field:'
                print >>self.logFile, self.dbg.hex_dump(map(lambda x: chr(x), packetData))
                buffer.nextDword()
                buffer.nextDword()

            unk5 = buffer.nextDword()
            #print >>self.logFile, str.format('unk5 = {0}', unk5)

            unk6 = buffer.nextDword()
            #print >>self.logFile, str.format('unk6 = {0}', unk6)

            unk7 = buffer.nextByte()
            #print >>self.logFile, str.format('unk7 = {0}', unk7)
            
            unk8 = buffer.nextDword()
            #print >>self.logFile, str.format('unk8 = {0}', unk8)

            
            if unk8 >= 2:

                unk8 = buffer.nextDword()

            dropped_by_entity = buffer.nextByte()
            if DEBUG:
                print >>self.logFile, str.format('dropped by player or mob : {0}', dropped_by_entity)
            
            itemId = buffer.nextDword()
            print >>self.logFile, 'itemId = ' + "0x%x"%(itemId&0xffffffff)

            #remaining = buffer.getRemainingBytes()
            
            itemName = getItemName(itemId)
            print >>self.logFile, str.format('itemName = {0}', itemName)

            if itemName == "unknown item":
                
                if DEBUG:
                    print ts + Style.BRIGHT + Fore.RED + 'UNKNOWN ITEM:' + "0x%x"%(itemId&0xffffffff)
                    unk_item = PlaySoundUnknownItem()
                    unk_item.start()

                print >>self.logFile, '---------------------------------'
                return

            if isCurrencyItem(itemName):
            
                if itemId !=0x50880BAF and itemId !=0x4F2B00ED: 
                    print ts + Style.BRIGHT + Fore.WHITE + 'CUR' + Style.NORMAL + str.format(': {0}',itemName)
                    number_of_orbs += 1

                    if ALERT_curr == True and SOUND_curr == True:
                    
                        if itemId == 0x61B2F5ED and SOUND_eternal == True: # Eternal Orb
                            crafting_drop = PlaySoundEternal()
                            crafting_drop.start()
                        if itemId == 0xC04F5629 and SOUND_exalted == True: # Exalted Orb
                            crafting_drop = PlaySoundExalted()
                            crafting_drop.start()
                        if itemId == 0x1C1E192B and SOUND_chroma == True: # Chromatic
                            crafting_drop = PlaySoundCraftingItem()
                            crafting_drop.start()
                        if itemId == 0x80047CFD and SOUND_divine == True: # Divine Orb
                            crafting_drop = PlaySoundDivine()
                            crafting_drop.start()
                        if itemId == 0x07A992EB and SOUND_gcp == True: # Gemcutter's Prism
                            crafting_drop = PlaySoundCraftingItem()
                            crafting_drop.start()
                        if itemId == 0xD8BD4F5D and SOUND_regal == True: # Regal Orb
                            crafting_drop = PlaySoundCraftingItem()
                            crafting_drop.start()
                        if itemId == 0x9B4B42A5 and SOUND_regret == True: # Orb of Regret
                            crafting_drop = PlaySoundCraftingItem()
                            crafting_drop.start()
                        if itemId == 0x716F588 and SOUND_vaal == True: #Vaal Orb
                            crafting_drop = PlaySoundVaal()
                            crafting_drop.start()
                        if itemId == 0x7353DDF9 and SOUND_chaos == True: # Chaos Orb
                            crafting_drop = PlaySoundChaos()
                            crafting_drop.start()
                        if itemId == 0x2D8E7632 and SOUND_blessed == True: # Blessed Orb
                            crafting_drop = PlaySoundCraftingItem()
                            crafting_drop.start()
                        if itemId == 0x7F0EF637 and SOUND_scour == True: # Orb of Scouring
                            crafting_drop = PlaySoundCraftingItem()
                            crafting_drop.start()
                        if itemId == 0x9110493F and SOUND_alc == True: # Orb of Alchemy
                            crafting_drop = PlaySoundCraftingItem()
                            crafting_drop.start()
                        if itemId == 0xC71BF58D and SOUND_fuse == True: # Orb of Fusing
                            crafting_drop = PlaySoundCraftingItem()
                            crafting_drop.start()
                        if itemId == 0xDC217297 and SOUND_chis == True: # Cartographer's Chisel
                            crafting_drop = PlaySoundCraftingItem()
                            crafting_drop.start()
                        if itemId == 0xC5732C85 and SOUND_chance == True: # Orb of Chance
                            crafting_drop = PlaySoundCraftingItem()
                            crafting_drop.start()
                        if itemId == 0xDD917991 and SOUND_jew == True: # Jeweller's Orb
                            crafting_drop = PlaySoundCraftingItem()
                            crafting_drop.start()
                        if itemId == 0xDD74C4BF and SOUND_glass == True: # Glassblower's Bauble
                            crafting_drop = PlaySoundCraftingItem()
                            crafting_drop.start()
                        if itemId == 0x79C23B15 and SOUND_mirror == True: # Mirror of Kalandra
                            crafting_drop = PlaySoundKalandra()
                            crafting_drop.start()
                        if itemId == 0x77662604 and SOUND_feather == True: # Albino Rhoa Feather
                            crafting_drop = PlaySoundFeather()
                            crafting_drop.start()
                        if itemId == 0xD7328257 and SOUND_alter == True: # Alteration Orb
                            crafting_drop = PlaySoundCraftingItem()
                            crafting_drop.start()
                        if itemId == 0xF5BB66E2 and SOUND_augmen == True: # Augmentation Orb
                            crafting_drop = PlaySoundCraftingItem()
                            crafting_drop.start()
                        if itemId == 0x4E6C4D33 and SOUND_trans == True: # Transmutation Orb
                            crafting_drop = PlaySoundCraftingItem()
                            crafting_drop.start()


                print >>self.logFile, '---------------------------------'
                return

                
            actual = buffer.nextByte()
            actual = buffer.nextByte()
            actual = buffer.nextByte()
            actual = buffer.nextByte() # required for 1.1.0 compatibility
            actual = buffer.nextDword() # required for 1.2.1b compatibility
            actual = buffer.nextDword()

            itemlevel = int(buffer.nextDword())
            if DEBUG:
                print >>self.logFile, str.format('itemlevel = {0}', itemlevel)

            if isGemItem(itemName) and ALERT_gems == True:

                if shouldNotify(itemName):
                    print ts + Fore.WHITE + str.format('SPC: {0}', itemName)
                    worker = PlaySoundSpecialItem()
                    worker.start()

                actual = buffer.nextByte()
                quality = int(buffer.nextDword())
                
                if quality >= gemqual:
                    print ts + Fore.CYAN + 'GEM' + Fore.WHITE + str.format(': {0}, quality: {1}',itemName,quality)
                    if SOUND_supgem == True:
                        supgem_drop = PlaySoundSuperiorGem()
                        supgem_drop.start()
                return

                if str(hex(itemId)) in specialGems and ALERT_specialgems == True:
                    print ts + Fore.CYAN + 'Special Gem' + Fore.WHITE +str.format(': {0}, quality: {1}',itemName,quality)
                    
                    if SOUND_specialgems == True:
                        spcgem_drop = PlaySoundHolyShit()
                        spcgem_drop.start()
                    
                print >>self.logFile, '---------------------------------'
                return

            if isFlaskItem(itemName) and ALERT_flask == True:
                actual = buffer.lastDword()
                quality = int(buffer.lastDword())
                actual = buffer.nextDword()
                actual = buffer.nextDword()
                actual = buffer.nextDword()
                rlvl = buffer.nextDword()
                ilvl = buffer.nextDword()

                #Granite Flask
                if itemId == 0x41BA1FB3 and ALERT_flask == True:
                    print ts + Style.BRIGHT + Fore.CYAN + 'Special Flask' + Style.NORMAL + Fore.WHITE + str.format(': {0}, quality: {1}, itemlvl: {2}',itemName,quality,ilvl)
                    if SOUND_graniteflask == True:
                        granflask_drop = PlaySoundGraniteFlask()
                        granflask_drop.start()

                if quality >= flaskqual:

                    print ts + Style.BRIGHT + Fore.CYAN + 'Flask' + Style.NORMAL + Fore.WHITE + str.format(': {0}, quality: {1}, itemlvl: {2}',itemName,quality,ilvl)

                    if SOUND_supflask == True:
                        supflask_drop = PlaySoundSuperiorFlask()
                        supflask_drop.start()
                print >>self.logfile, '--------------------------------'

                return

            req_lvl = buffer.nextDword()
            if DEBUG:
                print >>self.logFile, str.format('req lvl = {0}', req_lvl)
                
            if SEARCH_special:

                if isSearchItem(itemName,special_class):    

                    if  itemlevel >= special_ilvl and special_rlvl <= req_lvl:
                    
                        sound = PlaySound6Sockets()
                        sound.start()
                        print ts + Style.BRIGHT + Fore.RED + str.format('Special Item: {0}, itemlevel: {1}',itemName,itemlevel)
            
            actual = buffer.nextDword()
            actual = buffer.nextByte()
            rarity = buffer.nextDword()
            if DEBUG:
                print >>self.logFile, str.format('rarity = {0}', rarity)

            identified = buffer.nextDword()
            if DEBUG:
                print >>self.logFile, str.format('identified = {0}', identified)
                
            
            if isMapItem(itemName) and ALERT_maps == True:
                
                #if shouldNotify(itemName):
                    #print ts + Fore.WHITE + str.format('SPC: {0}', itemName)
                    #worker = PlaySoundSpecialItem()
                    #worker.start()

                actual = buffer.nextDword()
                actual = buffer.nextDword()
                actual = buffer.nextByte()
                
                actual = buffer.nextDword()
                actual = buffer.nextDword()
                quality = int(buffer.nextDword())
                actual = int(buffer.nextByte())
                if DEBUG:
                    print >>self.logFile, str.format('quality = {0}', quality)
                
                if quality == 0 and actual == 0:
                    quality = int(buffer.nextDword())
                    if DEBUG:
                        print >>self.logFile, str.format('quality new = {0}', quality)
                
                print ts + Style.BRIGHT + Fore.CYAN + 'MAP' + Style.NORMAL + Fore.WHITE + str.format(': {0}, rarity: {1}, itemlevel: {2}, quality: {3}',itemName,rarity,itemlevel,quality)

                print >>self.logFile, '---------------------------------'

                if SOUND_maps == True:
                    map = PlaySoundMaps()
                    map.start()
       
                number_of_maps += 1
                return

            if isBeltItem(itemName):
                
                if shouldNotify(itemName) and rarity == 0:
                    print ts + Fore.WHITE + str.format('SPC: {0}', itemName)
                    worker = PlaySoundSpecialItem()
                    worker.start()                
                
                if rarity == 3:
                
                    print ts + Style.NORMAL + Fore.YELLOW + 'UNI BELT' + Fore.YELLOW + str.format(': {0}, rarity: {1}, itemlevel: {2}',itemName,rarity,itemlevel)
                    if SOUND_uniques == True:
                        unique = PlaySoundUnique()
                        unique.start()

                    number_of_uniques += 1
            
                elif rarity == 2 and ALERT_rares == True:
                
                    print ts + Style.BRIGHT + Fore.YELLOW + 'BELT' + Style.NORMAL + Fore.WHITE + str.format(': {0}, rarity: {1}, itemlevel: {2}',itemName,rarity,itemlevel)

                    if SOUND_rares == True:
                        worker = PlaySoundWorker()
                        worker.start()

                print >>self.logFile, '---------------------------------'
                
                return

            if isQuestItem(itemName) and ALERT_quest == True:

                print ts + Style.BRIGHT + Fore.GREEN + 'Quest Item' + Style.NORMAL + Fore.WHITE + str.format(': {0}',itemName)
                if SOUND_quest == True:
                    quest = PlaySoundQuest()
                    quest.start()
                print >>self.logfile, '--------------------------------'
                return


            if isQuiverItem(itemName):

                if shouldNotify(itemName) and rarity == 0:
                    print ts + Fore.WHITE + str.format('SPC: {0}', itemName)
                    worker = PlaySoundSpecialItem()
                    worker.start()                
                
                if rarity == 3:
                
                    print ts + Style.NORMAL + Fore.YELLOW + 'UNI Quiver' + Fore.YELLOW + str.format('UNI Quiver: {0}, rarity: {1}, itemlevel: {2}',itemName,rarity,itemlevel)
                    if SOUND_uniques == True:
                        unique = PlaySoundUnique()
                        unique.start()

                    number_of_uniques += 1
            
                elif rarity == 2 and ALERT_rares == True:
                
                    print ts + Style.BRIGHT + Fore.YELLOW + 'QUIV' + Style.NORMAL + Fore.WHITE + str.format(': {0}, rarity: {1}, itemlevel: {2}',itemName,rarity,itemlevel)
                    
                    if SOUND_rares == True:
                        worker = PlaySoundWorker()
                        worker.start()

                print >>self.logFile, '---------------------------------'
                
                return

            if isJewelleryItem(itemName):
                # 0x29F77698 = gold ring
                # 0xDE069771 = gold amulet

                _implicitmods = int(buffer.nextDword())
                _mod = buffer.nextDword()
                _modvalues = buffer.nextByte()
                _modvalue = int(buffer.nextDword())

                if shouldNotify(itemName) and rarity == 0:
                        print ts + Fore.WHITE + 'SPC:' + str.format(' {0}', itemName)
                        worker = PlaySoundSpecialItem()
                        worker.start()
                
                if rarity == 2 and ALERT_rares == True:

                    print ts + Style.BRIGHT + Fore.YELLOW + 'JEW' + Style.NORMAL + Fore.WHITE + str.format(': {0}, rarity: {1}',itemName,rarity)
                    number_of_rares += 1

                    if SOUND_rares == True:
                        worker = PlaySoundWorker()
                        worker.start()
                    
                if (rarity == 0) and (itemId == 0x29F77698 or itemId == 0xDE069771) and ALERT_jewellry == True:
  
                    if _modvalue >= ringmf and itemId == 0x29F77698:
                        if ringmf >= 0:
                            print ts + Fore.WHITE + str.format('JEW: {0}, Value: {1}',itemName,_modvalue)
                        
                    if _modvalue >= amuletmf and itemId == 0xDE069771:
                        if amuletmf >= 0:
                            print ts + Fore.WHITE + str.format('JEW: {0}, Value: {1}',itemName,_modvalue)


                elif (rarity == 1) and (itemId == 0x29F77698 or itemId == 0xDE069771) and ALERT_jewellry == True:

                    if _modvalue >= ringmf and itemId == 0x29F77698:
                        if ringmf >= 0:
                            print ts + Fore.WHITE + 'JEW' + Fore.CYAN + str.format(': {0}, Value: {1}', itemName, _modvalue)

                    if _modvalue >= amuletmf and itemId == 0xDE069771:
                        if amuletmf >= 0:
                            print ts + Fore.WHITE + 'JEW' + Fore.CYAN + str.format(': {0}, Value: {1}', itemName, _modvalue)
                    
                elif (rarity == 2) and (itemId == 0x29F77698 or itemId == 0xDE069771) and ALERT_jewellry == True and ALERT_rares == False:

                    if _modvalue >= ringmf and itemId == 0x29F77698:
                        if ringmf >= 0:
                            print ts + Fore.WHITE + 'JEW' + Fore.YELLOW + str.format(': {0}, Value: {1}', itemName, _modvalue)

                    if _modvalue >= amuletmf and itemId == 0xDE069771:
                        if amuletmf >= 0:
                            print ts + Fore.WHITE + 'JEW' + Fore.YELLOW + str.format(': {0}, Value: {1}', itemName, _modvalue)

                elif rarity == 3:
                
                    if identified == 1 and itemId == 0x29F77698:
                        
                        if RUN == False:
                            RUN = True
                            number_of_uniques = 0
                            number_of_rares = 0
                            number_of_orbs = 0
                            number_of_maps = 0
                            
                            staTime= datetime.datetime.today().strftime("%Y-%m-%d %H:%M:%S")
                            startTime = str.format('{0}',staTime)
                            
                            print ts + Style.BRIGHT + Fore.GREEN + str.format('==== {0} ===== RUN started ===========',startTime)
                            
                            #print >>self.logFile, str.format('{0}', startTime)
                            # measure runtime later on here, get the start time
                            
                        else:
                        
                            RUN = False
                            stoTime = datetime.datetime.today().strftime("%Y-%m-%d %H:%M:%S")
                            stopTime = str.format('{0}',stoTime)
                            print ts + Style.BRIGHT + Fore.GREEN + str.format('==== {0} ===== RUN stopped ===========',stopTime)                            
                            print >>self.statFile, str.format('{0},{1},{2},{3},{4}', stopTime,number_of_uniques,number_of_rares,number_of_orbs,number_of_maps)
                            
                    else:
                        print ts + Style.NORMAL + Fore.YELLOW + 'UNI JEW' + Style.BRIGHT + Fore.YELLOW + str.format(': {0}, rarity: {1}, itemlevel: {2}',itemName,rarity,itemlevel)
                        if SOUND_uniques == True:
                            unique = PlaySoundUnique()
                            unique.start()
                        
                        number_of_uniques += 1

                        
                print >>self.logFile, '---------------------------------'
                return

            if isArmourItem(itemName):
                    
                if rarity == 0:

                    if shouldNotify(itemName):
                        print ts + Fore.WHITE + str.format('SPC: {0}', itemName)
                        worker = PlaySoundSpecialItem()
                        worker.start()

                        print >>self.logFile, '---------------------------------'
                        return
                        
                elif rarity == 2:
                    number_of_rares += 1
                
                if identified == 1:

                    if isShieldItem(itemName):
                    
                        if DEBUG:
                            print >>self.logFile, str.format('identified as SHIELD', '1')

                        _implicitmod = int(buffer.nextDword())
                        
                        if _implicitmod == 2:
                            actual=buffer.nextDword()
                                
                            _skipnext = int(buffer.nextByte())

                            if _skipnext >= 1:
                                actual=buffer.nextDword()

                                _impl_mod = int(buffer.nextDword())
                                if DEBUG:
                                    print >>self.logFile, str.format('implicit mod = {0}', _impl_mod)

                                _impl_mod_values = int(buffer.nextByte())
                                
                                for i in range(0,_impl_mod_values):
                                    # needs to be worked out
                                    _mod_value = int(buffer.nextDword())
                                    
                                _skipnext = int(buffer.nextByte())
                                    
                                if _skipnext >= 1:
                                    for i in range(0,_skipnext):
                                        actual = int(buffer.nextDword())

                                    
                        else:
                        
                            actual=buffer.nextDword()
                        
                            _skipnext = int(buffer.nextByte())            

                            if _skipnext >= 1:                        
                                for i in range(0,_skipnext):
                                    actual=buffer.nextDword()
                    
                                _skipnext = int(buffer.nextByte())            

                                if _skipnext >= 1:                        
                                    for i in range(0,_skipnext):
                                        actual=buffer.nextDword()
                    
                    else:
                        _implicitmods = int(buffer.nextDword())
                        
                        if _implicitmods == 2:
                        
                            if DEBUG:
                                print >>self.logFile, str.format('_implicitmods = {0}', _implicitmods)
            
                        elif _implicitmods == 1:

                            _implicitmod = buffer.nextDword()
                            if DEBUG:
                                print >>self.logFile, str.format('_implicitmod = {0}', _implicitmod)
                            
                            _impl_mod_values = int(buffer.nextByte())
                            if DEBUG:
                                print >>self.logFile, str.format('_impl_mod_values = {0}', _impl_mod_values)
                            
                            
                            for i in range(0,_impl_mod_values):
                                # needs to be worked out
                                _mod_value = int(buffer.nextDword())
                                
                        unk_mod=int(buffer.nextByte())
                        
                        if unk_mod >=0:
                            for i in range(0,unk_mod):
                                # needs to be worked out
                                _mod_value = int(buffer.nextDword())
                            
                    
                    number_of_mods = int(buffer.nextDword())

                else:
                
                    if isShieldItem(itemName):
                    
                        if DEBUG:
                            print >>self.logFile, str.format('identified as SHIELD', '1')
                    
                
                        _implicitmod = int(buffer.nextDword())
                        
                        if _implicitmod == 2:
                        
                            actual=buffer.nextDword()
                                
                            _skipnext = int(buffer.nextByte())

                            if _skipnext >= 1:
                                actual=buffer.nextDword()

                                _impl_mod = int(buffer.nextDword())
                                
                                if DEBUG:
                                    print >>self.logFile, str.format('implicit mod = {0}', _impl_mod)

                                _impl_mod_values = int(buffer.nextByte())

                                if DEBUG:
                                    print >>self.logFile, str.format('_impl_mod_values = {0}', _impl_mod_values)
                                
                                
                                if _impl_mod_values > 0:
                                    for i in range(0,_impl_mod_values):
                                        # needs to be worked out
                                        _mod_value = int(buffer.nextDword())
                                        if DEBUG:
                                            print >>self.logFile, str.format('_mod_value = {0}', _mod_value)

                        elif _implicitmod == 1:
                        
                            actual=buffer.nextDword()
                        
                            _skipnext = int(buffer.nextByte())            

                            if _skipnext >= 1:                        
                                for i in range(0,_skipnext):
                                    actual=buffer.nextDword()

                    else:

                        _implicitmods = int(buffer.nextDword())
                        
                        if _implicitmods == 2:
                        
                            if DEBUG:
                                print >>self.logFile, str.format('_implicitmods = {0}', _implicitmods)
            
                        elif _implicitmods == 1:

                            _implicitmod = buffer.nextDword()
                            if DEBUG:
                                print >>self.logFile, str.format('_implicitmod = {0}', _implicitmod)
                            
                            _impl_mod_values = int(buffer.nextByte())
                            if DEBUG:
                                print >>self.logFile, str.format('_impl_mod_values = {0}', _impl_mod_values)
                            
                            
                            for i in range(0,_impl_mod_values):
                                # needs to be worked out
                                _mod_value = int(buffer.nextDword())

                    number_of_mods = 0
                                
                if DEBUG:
                    print >>self.logFile, str.format('number of explicit mods = {0}', number_of_mods)
                
                if number_of_mods > 6:
                    print >>self.logFile, 'The last packet had an odd number_of_mods field:'
                    print >>self.logFile, self.dbg.hex_dump(map(lambda x: chr(x), packetData))

                    print >>self.logFile, '---------------------------------'                        
                    return
                
                
                for i in range(0,number_of_mods):
                    mod_id = buffer.nextDword()
                    if DEBUG:
                        print >>self.logFile, str.format('mod id = {0}', mod_id)
                    sub_mod_count = buffer.nextByte()
                    if sub_mod_count > 3:
                        print >>self.logFile, 'The last packet had an odd sub_mod_count field:'
                        if DEBUG:
                            print >>self.logFile, self.dbg.hex_dump(map(lambda x: chr(x), packetData))

                        print >>self.logFile, '---------------------------------'                        
                        return
                    
                    for ii in range(0,sub_mod_count):
                        mod_value=buffer.nextDword()
                        if sub_mod_count == 1:
                            if DEBUG:
                                print >>self.logFile, str.format('Modifier: {0}{1}', mod_value, getModifierName(mod_id))
                        elif sub_mod_count == 2:
                            if mod_value != 0:
                                sub_mods = getModifierName(mod_id).split("/")
                                sub_mod = sub_mods[ii]
                                if DEBUG:
                                    print >>self.logFile, str.format('Modifier: {0}{1}', mod_value, sub_mod)
                    
                quality = buffer.nextDword()
                if DEBUG:
                    print >>self.logFile, str.format('quality = {0}', quality)

                #Superior Quality modifier
                if quality >= whiteitemqual and ALERT_quality == True:
                    if rarity == 0:
                        print ts + Style.BRIGHT + Fore.WHITE + 'SUP' + Style.NORMAL + Fore.WHITE + str.format(': {0}, quality: {1}, itemlevel: {2}',itemName,quality,itemlevel)
                
                if quality >= rarityqual and ALERT_quality == True and rarityqual != 0:
                    if rarity == 1:
                        print ts + Style.BRIGHT + Fore.CYAN + 'SUP' + Style.NORMAL + Fore.WHITE + str.format(': {0}, quality: {1}, itemlevel: {2}',itemName,quality,itemlevel)
                    if rarity == 2:
                        print ts + Style.BRIGHT + Fore.YELLOW + 'SUP' + Style.NORMAL + Fore.WHITE + str.format(': {0}, quality: {1}, itemlevel: {2}',itemName,quality,itemlevel)

                    if ALERT_quality == True and SOUND_superior == True:
                        sup = PlaySoundSup()
                        sup.start()

                if quality >= rarityqual and rarity == 3:
                    print ts +Style.NORMAL + Fore.YELLOW + 'SUP' +Style.NORMAL + Fore.WHITE + str.format(': {0}, quality: {1}, itemlevel: {2}',itemNam,quality,itemlevel)
                    unique = PlaySoundUnique()
                    unique.start()

                actual = buffer.nextByte()
                sockets = int(buffer.nextDword())
                if DEBUG:
                    print >>self.logFile, str.format('sockets = {0}', sockets)

                if sockets == 0 or sockets > 6:
                    print >>self.logFile, 'The last packet had an odd sockets field:'
                    if DEBUG:
                        print >>self.logFile, self.dbg.hex_dump(map(lambda x: chr(x), packetData))
                    
                    print >>self.logFile, '---------------------------------'                        
                    return
                
                _all_color = []
                
                sock_color_tmp = ""
                sock_color_tmp = getSocketColor(int(buffer.nextByte()))
                
                actual = buffer.nextByte()
                _all_color.append(sock_color_tmp)
                
                for i in range(1,sockets):
                    sock_color_tmp = getSocketColor(int(buffer.nextByte()))
                    actual = buffer.nextByte()
                    _all_color.append(sock_color_tmp)

                if DEBUG:
                    print >>self.logFile, str.format('socket colors = {0}', _all_color)
                
                #actual = buffer.nextByte()
                sock_fragments = int(buffer.nextDword())
                
                if DEBUG:
                    print >>self.logFile, str.format('socket fragments = {0}', sock_fragments)

                if sock_fragments > 6 or sock_fragments == 0:
                    print >>self.logFile, 'The last packet had an odd sock_fragments field:'
                    if DEBUG:
                        print >>self.logFile, self.dbg.hex_dump(map(lambda x: chr(x), packetData))

                    print >>self.logFile, '---------------------------------'                        
                    return
                
                listlen = len(_all_color)
                
                if sock_fragments == 1:
                    if DEBUG:
                        print >>self.logFile, str.format('Fully linked = {0}', sock_fragments)
                    ii=0
                    for i in range(1,listlen):
                        _all_color.insert(i+ii,"-")
                        ii=ii+1
                        
                else:
                    iii=0
                    maxfrag = 1
                    for i in range(0,sock_fragments):
                        frag = int(buffer.nextByte())
                        if frag > 1:
                            if maxfrag < frag:
                                maxfrag = frag
                            for ii in range(1,frag):
                                _all_color.insert(ii+iii,"-")
                                iii=iii+1
                            iii=iii+ii+1
                        else:
                            iii=iii+1
                
                socketsetup = ''.join(_all_color)
                
                if DEBUG:
                    print >>self.logFile, str.format('Socket Setup = {0}', socketsetup)

                rare_alerted = False 

                if ALERT_race:
                
                    if any(i in socketsetup for i in race_sockets):
                    
                        if rarity == 0:
                            print ts + Style.BRIGHT + Fore.RED + "Race item" + Style.BRIGHT + Fore.WHITE + str.format(': {0}, rarity: {1}, itemlevel: {2}',itemName,rarity,itemlevel)
                        if rarity == 1:
                            print ts + Style.BRIGHT + Fore.RED + "Race item" + Style.BRIGHT + Fore.CYAN + str.format(': {0}, rarity: {1}, itemlevel: {2}',itemName,rarity,itemlevel)
                        if rarity == 2:
                            print ts + Style.BRIGHT + Fore.RED + "Race item" + Style.BRIGHT + Fore.YELLOW + str.format(': {0}, rarity: {1}, itemlevel: {2}',itemName,rarity,itemlevel)
                            rare_alerted = True 

                        if SOUND_race:
                            sound = PlaySoundRace()
                            sound.start()

                
                if any(i in socketsetup for i in ('R-G-B','R-B-G','G-B-R','G-R-B','B-R-G','B-G-R','R-B-B-G','G-R-R-B','G-B-B-R','B-G-G-R','B-R-R-G','R-G-G-B')):
                
                    if rarity == 0:
                        print ts + Style.BRIGHT + Fore.RED + "R" + Style.BRIGHT + Fore.GREEN + "G" + Style.BRIGHT + Fore.CYAN + "B" + Style.NORMAL + Fore.WHITE + str.format(': {0}, rarity: {1}, itemlevel: {2}',itemName,rarity,itemlevel)
                    if rarity == 1:
                        print ts + Style.BRIGHT + Fore.RED + "R" + Style.BRIGHT + Fore.GREEN + "G" + Style.BRIGHT + Fore.CYAN + "B" + Style.BRIGHT + Fore.CYAN + str.format(': {0}, rarity: {1}, itemlevel: {2}',itemName,rarity,itemlevel)
                    if rarity == 2:
                        print ts + Style.BRIGHT + Fore.RED + "R" + Style.BRIGHT + Fore.GREEN + "G" + Style.BRIGHT + Fore.CYAN + "B" + Style.BRIGHT + Fore.YELLOW + str.format(': {0}, rarity: {1}, itemlevel: {2}',itemName,rarity,itemlevel)
                        rare_alerted = True 

                    if SOUND_rgb:
                        sound = PlaySoundRGB()
                        sound.start()

                    
                msg = ""
                if sockets == 5:
                    if sock_fragments == 1:
                        msg = "5-SLOT 5-LINK"
                if sockets == 6:
                    if sock_fragments == 1:
                        msg = "6-SLOT 6-LINK"
                    elif sock_fragments == 2:
                        if maxfrag == 5:
                            msg = "6-SLOT 5-LINK"
                        else:
                            msg = "6-SLOT"
                    else:
                        msg = "6-SLOT"
                
                if msg != "":
                    print >>self.logFile, msg

                    if rarity == 0:
                        print ts + Style.BRIGHT + Fore.MAGENTA + msg + Style.BRIGHT + Fore.WHITE + str.format(' : {0}, rarity: {1}, ilvl: {2}, qual: {3}, sockets: {4}',itemName,rarity,itemlevel,quality, socketsetup)
                    if rarity == 1:
                        print ts + Style.BRIGHT + Fore.MAGENTA + msg + Style.BRIGHT + Fore.CYAN + str.format(' : {0}, rarity: {1}, ilvl: {2}, qual: {3}, sockets: {4}',itemName,rarity,itemlevel,quality, socketsetup)
                    if rarity == 2:
                        print ts + Style.BRIGHT + Fore.MAGENTA + msg + Style.BRIGHT + Fore.YELLOW + str.format(' : {0}, rarity: {1}, ilvl: {2}, qual: {3}, sockets: {4}',itemName,rarity,itemlevel,quality, socketsetup)
                        rare_alerted = True 

                    if SOUND_slots == True and msg == "5-SLOT 5-LINK" and ALERT_5slot5link == True:
                        sound = PlaySoundFiveLink()
                        sound.start()
                    elif SOUND_slots == True and msg == "6-SLOT 5-LINK" and ALERT_6slot5link == True:
                        sound = PlaySoundSixSlotFiveLink()
                        sound.start()
                    elif SOUND_slots == True and msg == "6-SLOT 6-LINK" and ALERT_6slot6link == True:
                        sound = PlaySoundSixLink()
                        sound.start()
                    elif SOUND_slots == True and msg == "6-SLOT" and ALERT_6slot == True:
                        sound = PlaySound6Sockets()
                        sound.start()
                        
                if rarity == 3:
                    print >>self.logFile, 'UNIQUE !'
                    number_of_uniques += 1
                    
                    print ts + Style.NORMAL + Fore.YELLOW + 'UNI' + Style.BRIGHT + Fore.YELLOW + str.format(': {0}, rarity: {1}, ilvl: {2}, qual: {3}, sockets: {4}',itemName,rarity,itemlevel,quality, socketsetup)
                    
                    if SOUND_uniques == True:
                        unique = PlaySoundUnique()
                        unique.start()

                if rarity == 2 and ALERT_rares == True:
                
                    print ts + Style.BRIGHT + Fore.YELLOW + 'RARE' + Style.NORMAL + Fore.WHITE + str.format(': {0}, rarity: {1}, itemlevel: {2}',itemName,rarity,itemlevel)

                    if SOUND_rares == True:
                        worker = PlaySoundWorker()
                        worker.start()
                    
                    
                print >>self.logFile, '---------------------------------'
                return
                    
            
            print >>self.logFile, '---------------------------------'            
                
        except: pass

    def afterDemanglingPacket(self, dbg):
        if self.lastPacketBufferAddress != 0 and self.lastPacketSize > 1:
            packetData = dbg.read_process_memory(self.lastPacketBufferAddress, self.lastPacketSize)
            packetData = map(lambda x: ord(x), packetData)
            if DEBUG_ALL:
                print >>self.logFile, packetData[0:4]
                if packetData[0:2] != [0x45, 0x00] and packetData[0:3] != [0xf1, 0x00, 0x00]:
                    print >>self.logFile, '_____________ new packet ________________________________________________________________________________________________________'
                    print >>self.logFile, str.format('Packet size: {0}',self.lastPacketSize)
                    print >>self.logFile, packetData
                    print >>self.logFile, self.dbg.hex_dump(map(lambda x: chr(x), packetData))
                    print >>self.logFile, '__________________________________________________________________________________________________________________________________'
            for i in range(self.lastPacketSize):
                if packetData[i:i+5] == [0xf0, 0x54, 0x92, 0x8a, 0x3a]:
                    if DEBUG:
        
                        print >>self.logFile, '---------------------------------'                    
                        print >>self.logFile, 'loot packet:'
                        print >>self.logFile, '   0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f'
                        print >>self.logFile, self.dbg.hex_dump(map(lambda x: chr(x), packetData[i:]))
                        
                    self.parseWorldItemPacket(packetData[i:])
        
            # player joined the area packet
            if packetData[0:4] == [0x08, 0x75, 0xff, 0x00]:
                if DEBUG_ALL:
                    self.playerJoined(packetData)

            # player left the area packet
            if packetData[0:4] == [0x08, 0x74, 0xff, 0x00]:
                if DEBUG_ALL:
                    self.playerLeft(packetData)

            # player chat packet
            if packetData[0:2] == [0x07, 0x00]:
                if DEBUG_ALL:
                    self.playerChat(packetData)
        
        return DBG_CONTINUE

    def getProcessId(self):
    
        if not STEAM:
            clients = [x[0] for x in self.dbg.enumerate_processes() if x[1].lower() == 'pathofexile.exe']
            print >>self.logFile, str.format('"pathofexile.exe" processes found: {0!s}', clients)
        else:
            clients = [x[0] for x in self.dbg.enumerate_processes() if x[1].lower() == 'pathofexilesteam.exe']
            print >>self.logFile, str.format('"pathofexilesteam.exe" processes found: {0!s}', clients)


        pid = None
        
        if not STEAM:
            if not clients or len(clients) == 0:
                print '' 
                print '-----------------------------------------'
                print Style.BRIGHT + Fore.YELLOW + 'No "PathOfExile.exe" process found.'
                print 'Did you set the STEAM flag properly?'
                print '-----------------------------------------'
                print ''
            elif len(clients) > 1: 
                print 'Found more than one "pathofexile.exe" process.'
            else: 
                pid = clients[0]
        else:
            if not clients or len(clients) == 0:
                print '' 
                print '-----------------------------------------'
                print Style.BRIGHT + Fore.YELLOW + 'No "PathOfExileSteam.exe" process found.'
                print 'Did you set the STEAM flag properly?'
                print '-----------------------------------------'
                print ''
            elif len(clients) > 1: 
                print 'Found more than one "pathofexilesteam.exe" process.'
            else: 
                pid = clients[0]
        
        return pid

    def getBaseAddress(self):
        if not STEAM:
            base = [x[1] for x in self.dbg.enumerate_modules() if x[0].lower() == 'pathofexile.exe'][0]
        else:
            base = [x[1] for x in self.dbg.enumerate_modules() if x[0].lower() == 'pathofexilesteam.exe'][0]
            
        print >>self.logFile, str.format('Base address: 0x{0:08x}', base)
        return base

    def run(self):
        print >>self.logFile, str.format('bp0: 0x{0:08x}: {1}', ItemAlert.BP0, self.dbg.disasm(ItemAlert.BP0))
        print >>self.logFile, str.format('bp1: 0x{0:08x}: {1}', ItemAlert.BP1, self.dbg.disasm(ItemAlert.BP1))
        print >>self.logFile, str.format('bp2: 0x{0:08x}: {1}', ItemAlert.BP2, self.dbg.disasm(ItemAlert.BP2))
        try:
            self.dbg.bp_set(ItemAlert.BP0, handler=self.grabPacketSize)
            self.dbg.bp_set(ItemAlert.BP1, handler=self.beforeDemanglingPacket)
            self.dbg.bp_set(ItemAlert.BP2, handler=self.afterDemanglingPacket)
        except Exception as inst:
            print >>self.logFile, type(inst)
            print >>self.logFile, inst.args
            print >>self.logFile, inst
            traceback.print_exc(file=self.logFile)
        print >>self.logFile, 'Starting main loop.'
        try: self.dbg.debug_event_loop()
        except: pass

    def atExit(self):
        #print ''
        #print 'atExit()'
        #print ''
        try: self.dbg.detach()
        except: pass

def checkVersion():
    if ctypes.sizeof(ctypes.c_voidp) != 4:
        print 'This program only works with a 32-bit Python installation!'
        print 'The preferred (tested) version is Python 2.7, 32-bit.'
        print 'You can download it from here: http://www.python.org/ftp/python/2.7.6/python-2.7.6.msi'
        sys.exit(1)

def main():
    
    if not STEAM:
        OMGDONT('title Path of Exile ItemAlert')
    else:
        OMGDONT('title Path of Exile (Steam) ItemAlert')
        
    checkVersion()
    
    if not STEAM:
        print Style.BRIGHT + Fore.YELLOW + str.format('Starting ItemAlert {0} for Path of Exile {1}', ALERT_VERSION, POE_VERSION)
        print ''
    else:
        print Style.BRIGHT + Fore.YELLOW + str.format('Starting ItemAlert {0} for Path of Exile (Steam) {1}', ALERT_VERSION, POE_VERSION)
        print ''
    with ItemAlert() as alerter: alerter.run()

if __name__ == '__main__':
    main()

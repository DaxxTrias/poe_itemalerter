#!/usr/bin/env python
# -*- coding: ISO-8859-1 -*-

'''
sku wrote this program. As long as you retain this notice you
can do whatever you want with this stuff. If we meet some day, and you think
this stuff is worth it, you can buy me a beer in return.
'''


import ItemList

def getSocketColor(socket_color):
    if socket_color == 1:
        return "R"
    elif socket_color == 2:
        return "G"
    elif socket_color == 3:
        return "B"


def shouldNotify(itemName):
    return True if not _filterItems else itemName in getNotifyItems()
    
def isGemItem(itemName):
    return True if not _filterItems else itemName in getGemItems()
    
def isFlaskItem(itemName):
    return True if not _filterItems else itemName in getFlaskItems()

def isArmourItem(itemName):
    return True if not _filterItems else itemName in getArmourItems()

def isCurrencyItem(itemName):
    return True if not _filterItems else itemName in getCurrencyItems()
    
def isMapItem(itemName):
    return True if not _filterItems else itemName in getMapItems()
    
def isJewelleryItem(itemName):
    return True if not _filterItems else itemName in getJewelleryItems()
    
def isShieldItem(itemName):
    return True if not _filterItems else itemName in getShieldItems()
    
def isBeltItem(itemName):
    return True if not _filterItems else itemName in getBeltItems()
    
def isQuiverItem(itemName):
    return True if not _filterItems else itemName in getQuiverItems()

def isQuestItem(itemName):
    return True if not _filterItems else itemName in getQuestItems()

def isSearchItem(itemName,itemClass):
    return True if not _filterItems else itemName in getSearchItems(itemClass)
    

    
    
def getSearchItems(itemClass):

    _searchItems = []
    keywords = itemClass
    for key in ItemList._items:
        if any(x in ItemList._items[key][2] for x in keywords): _searchItems.append(ItemList._items[key][1])

    return _searchItems
    
def getNotifyItems():
    return _notifyItems
    
def getGemItems():
    return _gemItems
    
def getFlaskItems():
    return _flaskItems

def getArmourItems():
    return _armourItems
    
def getCurrencyItems():
    return _currencyItems
   
def getMapItems():
    return _mapItems

def getJewelleryItems():
    return _jewelleryItems

def getShieldItems():
    return _shieldItems

def getBeltItems():
    return _beltItems

def getQuiverItems():
    return _quiverItems

def getQuestItems():
    return _questItems

    
_quiverItems = []
quiverwords = ["Quivers"]
for key in ItemList._items:
    if any(x in ItemList._items[key][2] for x in quiverwords): _quiverItems.append(ItemList._items[key][1])
    
_questItems = []
questwords = ["QuestItems"]
for key in ItemList._items:
    if any(x in ItemList._items[key][2] for x in questwords): _questItems.append(ItemList._items[key][1])

_beltItems = []
beltwords = ["Belts"]
for key in ItemList._items:
    if any(x in ItemList._items[key][2] for x in beltwords): _beltItems.append(ItemList._items[key][1])
    
_shieldItems = []
shieldwords = ["Shields"]
for key in ItemList._items:
    if any(x in ItemList._items[key][2] for x in shieldwords): _shieldItems.append(ItemList._items[key][1])
    
_notifyItems = []
notifwords = ["Map", "Currency"]
for key in ItemList._items:
    if any(x in ItemList._items[key][2] for x in notifwords): _notifyItems.append(ItemList._items[key][1])

_gemItems = []
gemwords = ["Gems"]
for key in ItemList._items:
    if any(x in ItemList._items[key][2] for x in gemwords): _gemItems.append(ItemList._items[key][1])
    
_flaskItems = []
flaskwords = ["Flasks"]
for key in ItemList._items:
    if any(x in ItemList._items[key][2] for x in flaskwords): _flaskItems.append(ItemList._items[key][1])

_armourItems = []
armourwords = ["Armours", "Weapons"]
for key in ItemList._items:
    if any(x in ItemList._items[key][2] for x in armourwords): _armourItems.append(ItemList._items[key][1])

_currencyItems = []
currencywords = ["Currency"]
for key in ItemList._items:
    if any(x in ItemList._items[key][2] for x in currencywords): _currencyItems.append(ItemList._items[key][1])

_mapItems = []
mapwords = ["Map"]
for key in ItemList._items:
    if any(x in ItemList._items[key][2] for x in mapwords): _mapItems.append(ItemList._items[key][1])

_jewelleryItems = []
jewellerywords = ["Rings", "Amulets"]
for key in ItemList._items:
    if any(x in ItemList._items[key][2] for x in jewellerywords): _jewelleryItems.append(ItemList._items[key][1])
    
# === SETTINGS ===

# Set this to True if you want to filter items and only announce
# items that have been added to the _notifyItems list.
# If _filterItems is False, ItemAlertPoE will announce every item drop.
_filterItems = True

# Add items that you wish to announce to this list.
# This list is only considered if _filterItems is set to True.
# If the item name countains a single quote, either escape it
# using \' or use double quotes like in the example below.

#Flasks
#_notifyItems.append("Quicksilver Flask")
#_notifyItems.append("Granite Flask")

#Armors
#_notifyItems.append("Occultist's Vestment") #Shavronne's Wrappings
#_notifyItems.append("Nightmare Bascinet") #The Bringer of Rain / Devoto's Devotion
#_notifyItems.append("Murder Mitts") #Thunderfist
#_notifyItems.append("Glorious Plate") #Kaom's Heart
#_notifyItems.append("Champion Kite Shield") #Aegis Aurora
#_notifyItems.append("Archon Kite Shield") #Prism Guardian
#_notifyItems.append("Sinner Tricorne") #Alpha's Howl

#Weapons
#_notifyItems.append("Imperial Bow") #Lioneye's Glare / Windripper
#_notifyItems.append("Spine Bow") #Voltaxic Rift
#_notifyItems.append("Void Sceptre") #Mon'tregul's Grasp
#_notifyItems.append("Siege Axe") #Soul Taker

#Jewellery
#_notifyItems.append("Gold Ring") #Andvarius
#_notifyItems.append("Amethyst Ring") #Ming's Heart
#_notifyItems.append("Unset Ring") #Voideye
#_notifyItems.append("Lapis Amulet") #Stone of Lazhwar
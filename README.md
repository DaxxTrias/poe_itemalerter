#
## ItemAlertPoE � Item Alerter for Path of Exile

*	#ItemAlertPoE tracks all item drops in the game [Path of Exile](http://www.pathofexile.com)

*	It alerts you in the console of various items quality, rarity, or socket combinations. 

*	Sounds are also played for particularly valuable items 

#		Notes

*	#Currently maintained by [DaxxTrias](https://bitbucket.org/DaxxTrias/poe_itemalerter)

*	Recently managed by [Sarge](https://github.com/Sergej-PoE/POE_ItemAlerter)

*	PoERecvOffsetFinder.exe author: Spl3en

*	Original author: SKU


## Installation instructions
* Download and install Python 2.7 32 bit: [Py 2.7 32bit](http://www.python.org/ftp/python/2.7.6/python-2.7.6.msi)
* Download and install dependencies: [PyDbg 1.2 32bit for Py 2.7](http://www.lfd.uci.edu/~gohlke/pythonlibs/#pydbg)
* Download and uncompress ItemAlertPoE anywhere: [ItemAlerter](https://bitbucket.org/DaxxTrias/poe_itemalerter/downloads#tag-downloads)
* Open ItemAlertPoE.py (in /src folder) in you're text editor, around line 47 make sure you change the STEAM variable to true or false depending on you're game client.
* Run Path of Exile game client
* Finally, double click ItemAlertPoE.py (or launch.bat)


#Version History

	11-Sep-2014
		* Updated for 1.2.2

	6-Sep-2014
		* Updated for 1.2.1b

	8-May-2014
		* Updated for 1.1.3b
		* Minor code cleaning
		* Updated some debugging text

	1-May-2014
		* Updated for 1.1.3

	17-Apr-2014
		* Updated for 1.1.2b
	
	7-Apr-2014
		* Updated for 1.1.2
		* Itemlist additions
		* Fixed a bug when rarityqual was set to 0
	
	1-Apr-2014
		* Itemlist fixes
		* Additional control over JEW (Rarity) alerts
		* Additional control over SUP alerts (White only or Magic/Rare/Uni)
		* Special item alert fixes (for NotifyItems)
		* Added sound alert for Divine Orb & Vaal Orb (Thx clopman)
		* SPC Jew alerts clearer to read in console
		* Special items should no longer double notify

	31-Mar-2014
		* Updated for 1.1.1c2 (hotfix)
		* Itemlist additions and fixes
		* Additional notifications added to the notifylist, off by default.
		* SPC sound was disabled, it has since been re-enabled. (appended notification sounds)
		* Added a minimum quality for gold amulet/ring alert, similar to other modifiers. (more to come)
		* Fixed a bug with most items not properly alerting when specified to(such as rings)


	27-Mar-2014
		* Updated for 1.1.1c
		* ItemList++
		* Changed Map console alerts to be in line with previous changes for readability
		* Enabled alert sounds for special items in the notifyitems lower section.

	26-Mar-2014
		* ItemList++
		* Added an alert with sound for the Granite Flask.
		* Adjusted the ANSI Colors in the console log for easier readability
		* Added sound alerts for Aug/Trans/Alt orbs. Off by default.

	25-Mar-2014
		* Added an alert with sound for chromatic orbs (Thx for noticing homarid)
		* Added an alert with sound for unknown items (off by default)
		* Corrected the Flask sound alert so it declares that its a Superior
		* More code cleanup
		* ItemList++

	24-Mar-2014
		* Added an alert with sound for quest items (sound is off by default)
		* Added an alert with sound for superior flask drops
		* Additional packet parser functionality
		* ItemList++
		* Code Cleanup (Including a newly revamped config area)

	23-Mar-2014
		* Customized 5s/6s & link alerts for more control over what alerts trigger.
		* ItemList++
		* Changed Blue color text to Cyan for easier readability (Thx Sync for suggestion)

	21-Mar-2014
		* Additional alert sounds (Thx Clopman)
		* Itemlist++
		* Code cleanup
		* Added an alert sound for superior items (only if they meet your quality requirements)
		* Other misc stuff in preparation for future updates

	20-Mar-2014
		* Revamped alert sounds again (Thx Clopman)
		* Yet more ItemList changes (Additions, fixes)
		* Added Albino Rhoa Feather to the alert (Thx ReadyToKill)
		
	18-Mar-2014:
		* Updated for 1.1.1b
		* Revamped alert sounds (Thx sync)
		* Additional info for unique drops in the console (ilvl)
		* Yet more ItemList additions

	15-Mar-2014:
		* Updated for 1.1.1
		* More ItemList additions
		* Vaal Orb sound alert fix
		* Rare drop alert sounds optional flag added.

	11-Mar-2014:
		* Updated for 1.1.0F
		* Updated Itemlist with more additions, mostly 1.1 related
		* Added a quality modifier alert for superior items. (Thx CAHbl4)
		* Added timestamps to console logs (Thx CAHbl4)
		* Added alert for the new Vaal Orb (Thx Nipper)
		* Revamped Readme
		* QoL changes to the alerter (notify change adjustments, less text spam in console etc)


# Sergej' changes from 24-Apr-2013 - 23-Nov-2013:
	5 Link and 6 Link detection
	6 socket detection
	RGB Link detection
	Gem detection based on quality (adjust value in line 239: if quality >= 5:)
	Debug mode included, will log all properties of a drop. (DEBUG = True)

	configurable alerts (Rares, Gems,Maps, Currency, values of gold amulets and rings)
	logging functionality - drop an Andvarius to start logging, drop it again to stop logging. Logfile can be imported in excel.
	color coded output in the alert window
	can show mods and values of an identified item.
	added missing maps and gems  

# SKU's old version:
	**Unique** items detection (plays unique.wav)
	**Superior Gem** detection (plays superiorgem.wav)
	Non filtered items play sound drop.wav
	Added Multistrike Support Gem
	Added Cyclone Skill Gem
	Removed scrolls from beeping
	Bugfix: Now works with no C:\Windows OS installations

